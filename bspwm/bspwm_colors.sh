#!/usr/bin/env bash

bspc config focused_border_color "#7b8c4c"
bspc config normal_border_color  "#556034"
