let g:neovide_refresh_rate=300
let g:neovide_transparency=0.5
let g:neovide_cursor_vfx_mode = "pixiedust"
let g:neovide_cursor_animation_length=0.13
let g:neovide_cursor_trail_length=0.8
let g:neovide_cursor_vfx_particle_density=70.0
set guifont=Hack:h8
