" Find files using Telescope command-line sugar.
nnoremap <leader>tt :Telescope find_files<cr>
nnoremap <leader>tg :Telescope live_grep<cr>
nnoremap <leader>tb :Telescope buffers<cr>
nnoremap <leader>th :Telescope help_tags<cr>
