# list commands
alias ls="exa --color=always --group-directories-first"
alias l="exa -la --color=always --group-directories-first"
alias ls="exa --color=always --group-directories-first"
alias ll="exa -l --color=always --group-directories-first"
alias la="exa -la --color=always --group-directories-first"

# remove commands
alias rm="trash -i"
alias rf="rm -rf"

# confirm before overwriting stuff
alias cp="cp -i"
alias mv="mv -i"

# some additional flags
alias df="df -h"
alias free="free -m"

# ps commands
alias psa="ps auxf"
alias psgrep="ps aux | grep -v grep | grep -i -e VSZ -e"
alias psmem="ps auxf | sort -nr -k 4"
alias pscpu="ps auxf | sort -nr -k 3"

# error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# clear commands
alias clear="~/.config/zsh/clearcommand.sh"
alias claer="clear"
alias cls="clear"
alias clearl="clear; la"
alias cll="clear;ll"
alias clearall="nextbackground; /bin/clear; colorscript random; echo ; fastfetch"
alias {clsall,cleall,cla}="/bin/clear; colorscript random; echo ; fastfetch"

# different fetches
alias clea="/bin/clear; colorscript random"
alias clef="/bin/clear; fastfetch"
alias clen="/bin/clear; neofetch"
alias cler="/bin/clear; rxfetch"
alias clep="/bin/clear; pfetch"

# pacman and yay update etc.
alias autorm="sudo pacman -Rcns $(pacman -Qdtq)" 
alias pacsyu="sudo pacman -Syyu"
alias yaysyu="yay -Syu --noconfirm"  

# quick access scripts
alias zshrc="vim ~/.zshrc"
alias zshenv="vim ~/.zshenv"
alias aliasrc="vim ~/.aliasrc"
alias startuprc="vim ~/.config/zsh/startup.sh"
alias sxhkdrc="vim ~/.config/sxhkd/sxhkdrc"
alias bspwmrc="vim ~/.config/bspwm/bspwmrc"
alias qtilerc="vim ~/.config/qtile/config.py"
alias nvimrc="vim ~/.config/nvim/init.vim"
alias polybarrc="vim ~/.config/polybar/config"
alias picomrc="vim ~/.config/picom/picom.conf"
alias alacrittyrc="vim ~/.config/alacritty/alacritty.yml"

# find/grep command
alias findeverywhere="find /. | grep"

# color mode
alias grep="grep --color=auto"
alias egrep="egrep --color=auto"
alias fgrep="fgrep --color=auto"

# volumecontrol
alias mute="amixer sset Master mute"
alias unmute="amixer sset Master unmute; pactl set-sink-mute 0 0"
alias louder="amixer set Master 5%+ unmut"
alias quieter="amixer set Master 5%- unmut"

# "new" commands/easier access/easier to remember
alias sizeofall="du -shc ./*"
alias battery="cat /sys/class/power_supply/BAT0/capacity"
alias archey="archey3"
alias neofetch="fastfetch"

alias newbackground="wpg -s ~/.config/wallpapers"
alias nextbackground="wpg -s ~/.config/wallpapers; pywal-discord -t abou"
alias netmgr="networkmanager_dmenu"
alias py="python3"

# so that all aliases also work with sudo
alias sudo="sudo "

# vim as default
alias vi="nvim"
alias vim="nvim"
alias svi="sudo vi"
alias vis="vim '+set si'"
alias edit="vim"
alias nano="vim "

# fun remaps
alias why="man "
alias fuck="killall -9 "
alias please="sudo "
alias fucking="sudo $(history -p !!)"

# shorted commands
alias :wq="exit"
alias :q="exit"
alias c="dbus-launch codium .;exit"
alias v="nvim ./"
alias n="nautilus .;exit"
alias cm="clipmenu"
alias diff="colordiff"
alias lol="lolcat"
alias open="xdg-open "
# alias s="startx"
alias sshlin="ssh -p 5040 172.105.79.115"

# simply usefull
alias reboot="sudo reboot"
alias poweroff="sudo poweroff"
alias shutdown="sudo shutdown"


# set layouts (s for setxkbmap)
alias sus="setxkbmap us"
alias scol="setxkbmap us -variant colemak"

# misstypes to sl
# Currently deactiaveted
alias sl="ls"
alias suod="sudo"
alias ext="exit"
alias xeit="exit"
alias exti="exit"
alias cim="vim"
alias vin="vim"
alias clac="calc"

# dumb stuff
alias discord="Discord"

# development
# Command line alias to start the browser-sync server
alias serve="browser-sync start --server --files ."
alias servejs="nodemon "
alias nvmsource="source ~/.nvm/nvm.sh"

# git commands
alias addup="git add -u"
alias addall="git add ."
alias gadd="git add "
alias branch="git branch"
alias checkout="git checkout"
alias clone="git clone"
alias commit="git commit -m"
alias fetch="git fetch"
alias merge="git merge"
alias pull="git pull origin"
alias push="git push origin"
alias stat="git status"  # status is protected so stat it is
alias glog="git log"  # log is also protected so glog it is
alias gdiff="git diff" # diff is its own command so gdiff it is
alias gsize="git gc;git count-objects -vH"

# just test it out its fine believe me...
alias rickroll="~/.config/zsh/roll.sh"
alias rr="~/.config/zsh/roll.sh"

alias parrot="curl parrot.live"

alias weather="curl wttr.in/Zollikofen"
